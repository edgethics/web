import Head from 'next/head'
import styles from './styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Coming Soon</title>
        <meta name="description" content="Website launching soon!" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Coming Soon!
        </h1>
        <p className={styles.description}>
          Our website is under construction. We&apos;ll be here soon with our new awesome site.
        </p>
      </main>
    </div>
  );
}
